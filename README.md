Class, H. and Kissinger, A. and Knopf, S. and Konrad, W. and Noack, V. and Scheer, D.,
Mehrstufige und regionale Charakterisierung potentieller CO2-Speicherformationen
unter besonderer Berücksichtigung von Soleaufstiegsrisiken - ein integrierter
natur- und sozialwissenschaftlicher Ansatz,
Schlussbericht CO2Brim (Förderkennzeichen 03G0802A, Bundesministerium für Bildung
und Forschung, Förderprogramm GEOTECHNOLOGIEN), Technischer Bericht WB04/2015, LH2/23, 2015

For your convenience please find the [BibTex entry here](https://git.iws.uni-stuttgart.de/dumux-pub/Kissinger2015a/class-etal2015.bib)

Installation
############

You can build the module just like any other DUNE module.
For building and running the executables, go to the folders containing
the sources listed above. For the basic dependencies see dune-project.org.

The easiest way is to use the installKissinger2015a.sh in this folder here.
You might want to look at it before execution [here](https://git.iws.uni-stuttgart.de/dumux-pub/Kissinger2015a/installKissinger2015a.sh). Create a new folder containing the script and execute it.

You might want to proceed as below in a terminal:
mkdir kissinger2015
cd kissinger2015
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Kissinger2015a/raw/master/Kissinger2015a.opts
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Kissinger2015a/raw/master/installKissinger2015a.sh
chmod u+x installKissinger2015a.sh
./installKissinger2015a.sh



- All matlab scripts and paraview state files used for creating the output in the report can be found in the folder "results"
===============================

The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

  appl/co2/co2brim/complex,

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

  appl/co2/co2brim/complex/solling1p2cni.cc,
  appl/co2/co2brim/complex/solling2p.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.

===============================

When this module was created, the original module dumux-devel was using
the following list of DUNE modules and third-party libraries.
BEWARE: This does not necessarily mean that the applications in this
extracted module actually depend on all these components.

dumux...................: version 2.7-svn (/temp/kissinger/DUMUX/dumux)
dune-alugrid............: version 2.3 (/temp/kissinger/DUMUX/dune-alugrid)
dune-common.............: version 2.3.1 (/temp/kissinger/DUMUX/dune-common)
dune-geometry...........: version 2.3.1 (/temp/kissinger/DUMUX/dune-geometry)
dune-grid...............: version 2.3.1 (/temp/kissinger/DUMUX/dune-grid)
dune-istl...............: version 2.3.1 (/temp/kissinger/DUMUX/dune-istl)
dune-localfunctions.....: version 2.3.1 (/temp/kissinger/DUMUX/dune-localfunctions)
ALBERTA.................: no
ALUGrid.................: no
AmiraMesh...............: no
BLAS....................: yes
GMP.....................: yes
Grape...................: no
METIS...................: no
METIS...................: no
MPI.....................: yes (OpenMPI)
OpenGL..................: yes (add GL_LIBS to LDADD manually, etc.)
ParMETIS................: no
SIONLIB.................: no
SuperLU-DIST............: no
SuperLU.................: yes (version 4.3 or newer)
UG......................: yes (parallel)
UMFPACK.................: no
ZOLTAN..................: no
dlmalloc................: no ()
psurface................: no
zlib....................: version 1.2.8
