// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outlfow problem.
 */
#ifndef DUMUX_1P2CNI_OUTFLOW_SPATIAL_PARAMS_HH
#define DUMUX_1P2CNI_OUTFLOW_SPATIAL_PARAMS_HH

#if SPATIALPARAMS == 1
#include <dumux/material/spatialparams/implicitspatialparams1p.hh>
#elif SPATIALPARAMS == 2
#include <dumux/material/spatialparams/implicitspatialparams.hh>
#else
#include <dumux/material/spatialparams/fvspatialparams.hh>
#endif
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

#include "../helperclasses/vertidxtoelemneighbormapper.hh"

#include <dune/common/float_cmp.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class SollingSpatialParams;

namespace Properties
{

#if SPATIALPARAMS != 1
// The spatial parameters TypeTag
NEW_TYPE_TAG(SollingSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(SollingSpatialParams, SpatialParams, Dumux::SollingSpatialParams<TypeTag>);
// Set the material Law
SET_PROP(SollingSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffMaterialLaw> type;
};
#endif
}
/*!
 * \ingroup OnePTwoCBoxModel
 * \ingroup ImplicitTestProblems
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outflow problem.
 */
template<class TypeTag>
#if SPATIALPARAMS == 1
class SollingSpatialParams : public ImplicitSpatialParamsOneP<TypeTag>
#elif SPATIALPARAMS == 2
class SollingSpatialParams : public ImplicitSpatialParams<TypeTag>
#else
class SollingSpatialParams : public FVSpatialParams<TypeTag>
#endif
{
    // Parent Type
#if SPATIALPARAMS == 1
    typedef ImplicitSpatialParamsOneP<TypeTag> ParentType;
#elif SPATIALPARAMS == 2
    typedef ImplicitSpatialParams<TypeTag> ParentType;
#else
    typedef FVSpatialParams<TypeTag> ParentType;
#endif

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef Dune::GridPtr<Grid> GridPointer;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;

    typedef typename GridView:: template Codim<0>::Iterator ElementIterator;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld,
        pressureIdx = Indices::pressureIdx,
    };
    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    //Vectors storing permeability and porosity values at every element for faster access
    typedef std::vector<Scalar> PermeabilityType;
    typedef std::vector<Scalar> PorosityType;
  //  typedef Dune::ReservoirPropertyCapillary<3> ReservoirProperties;
    typedef typename GET_PROP(TypeTag, ParameterTree) Params;

#if SPATIALPARAMS != 1
    //get the material law from the property system
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;
#endif

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef Dumux::VertIdxToElemNeighborMapper<GridView> VertIdxToElemNeighborMapper;
    typedef typename VertIdxToElemNeighborMapper::NeighborElementSeedsIterator NeighborElementSeedsIterator;
    //typedef LinearMaterial<Scalar> EffMaterialLaw;
public:

    enum {
        //Layer indices
        quarIdx=0,
        terIdx=1,
        rcIdx=2,
        oliIdx=3,
        obsIdx=4,
        mbsIdx=5,
        solIdx=6,
        umbsIdx=7,
        ubsIdx=8,
        zecIdx=9,
        kreideIdx=10,
        tzIdx=11,

        //Flux line indices
        noFluxIdx=0,
        rupelFluxIdx=1,
    };

    enum {
        //indices of element parameters specified in dgf file
        fluxElemIdx = 0,
        layerEIdx = 1,
        riverElemIdx = 2,
    };

    enum {
        //indices of element markers to be used with elementMarkerMap_
        eIdxTZ = 0,
        eIdxRiver = 1,

        vIdxSB = 1,
        vIdxTerQuar = 2,
        vIdxRiver = 3,
    };

    SollingSpatialParams(const GridView &gridView)
        : ParentType(gridView), gridView_(gridView), vertexElementMapper_(gridView)
    {

        Scalar initialPermeability(0);
        try
        {
            topOfReservoir_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.TopOfReservoir);
            Swr_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Swr);
            Snr_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Snr);
            brooksCoreyPe_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BrooksCoreyPe);
            brooksCoreyLambda_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BrooksCoreyLambda);
            compressibility_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Compressibility);
            initialPermeability_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.InitialPermeability);
            arrayPerm_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermQuar);
            arrayPerm_[terIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermTer);
            arrayPerm_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermRC);
            arrayPerm_[oliIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermOli);
            arrayPerm_[obsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermOBS);
            arrayPerm_[mbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermMBS);
            arrayPerm_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermSol);
            arrayPerm_[umbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermUMBS);
            arrayPerm_[ubsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermUBS);
            arrayPerm_[zecIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermZec);
            arrayPerm_[kreideIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermKreide);
            arrayPerm_[tzIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermTZ);
            arrayPor_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorQuar);
            arrayPor_[terIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorTer);
            arrayPor_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorRC);
            arrayPor_[oliIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorOli);
            arrayPor_[obsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorOBS);
            arrayPor_[mbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorMBS);
            arrayPor_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorSol);
            arrayPor_[umbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorUMBS);
            arrayPor_[ubsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorUBS);
            arrayPor_[zecIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorZec);
            arrayPor_[kreideIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorKreide);
            arrayPor_[tzIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorTZ);
            tzWidth_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.TzWidth);
            cellWidth_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.CellWidth);
            holesRupelClosed_ = GET_RUNTIME_PARAM(TypeTag, bool, SpatialParams.HolesRupelClosed);
            dirichletAtTop_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.DirichletAtTop);

            //get the initialization period
            initializationPeriod_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InitializationPeriod);
            //get the mass reduction factor
            massReductionFactor_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.MassReductionFactor);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown while reading in parameters in the spatial parameters file!\n";
            exit(1);
        }

#if SPATIALPARAMS != 1
        // residual saturations
        materialParams_.setSwr(Swr_);
        materialParams_.setSnr(Snr_);

        // parameters for the Brooks-Corey law
        materialParams_.setPe(brooksCoreyPe_);
        materialParams_.setLambda(brooksCoreyLambda_);
#endif
    }

    ~SollingSpatialParams()
    {}

    void setParams(GridPointer *gridPtr, Problem *problemPtr)
    {
        gridPtr_ = gridPtr;
        problemPtr_ = problemPtr;
        // find out which elements lie at the border of the salt wall
        initializeTransitionZone_();

        //Initialize the permeability and porosity.
        //Loop over all elements and obtain the permeability and porosity values from the gridPtr.
        //Store the values in the perm_ and porosity_ vectors.
        int numElems = gridView_.size(0);
        perm_.resize(numElems);
        porosity_.resize(numElems);
        ElementIterator eIt = gridView_.template begin<0>();
        const ElementIterator eEndIt = gridView_.template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            int eIdxGlobal = gridView_.indexSet().index(*eIt);
            int layerIdx = findLayerIdx(*eIt);
            if(layerIdx != tzIdx)
            {
                perm_[eIdxGlobal] = arrayPerm_[layerIdx];
                porosity_[eIdxGlobal] = arrayPor_[layerIdx];
            }
            //transition zone: return weighted arithmetic averaged permeability and porosity
            else
            {
                std::vector<Scalar> eParam = (*gridPtr_).parameters(*eIt);
                int origLayerIdx = (int)(eParam[layerEIdx] + 0.5) - 1;
                perm_[eIdxGlobal] = (tzWidth_*arrayPerm_[layerIdx] + (cellWidth_-tzWidth_)*arrayPerm_[origLayerIdx])/cellWidth_;
                porosity_[eIdxGlobal] = (tzWidth_*arrayPor_[layerIdx] + (cellWidth_-tzWidth_)*arrayPor_[origLayerIdx])/cellWidth_;
            }
        }

        //Check maximum and minimum values in perm_/porosity_ vectors
        Scalar maxPor = 0;
        Scalar minPor = 0;
        minPor = *std::min_element(porosity_.begin(), porosity_.end());
        maxPor = *std::max_element(porosity_.begin(), porosity_.end());

        Scalar maxPerm = 0;
        Scalar minPerm = 0;
        minPerm = *std::min_element(perm_.begin(), perm_.end());
        maxPerm = *std::max_element(perm_.begin(), perm_.end());

        if (gridView_.comm().size() > 1)
        {
            minPor = gridView_.comm().min(minPor);
            maxPor = gridView_.comm().min(maxPor);
            maxPerm = gridView_.comm().min(maxPerm);
            minPerm = gridView_.comm().min(minPerm);
        }

        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Minimum Perm: "<<minPerm<<" Maximum Perm: "<<maxPerm<<std::endl;
            std::cout<<"Minimum Porosity: "<<minPor<<" Maximum Porosity: "<<maxPor<<std::endl;
        }

    }

    /*!
     * \brief Update the spatial parameters with the flow solution
     *        after a timestep.
     *
     * \param globalSolution the global solution vector
     */
//    void update(const SolutionVector &globalSolution)
//    {
//    };

    /*!
     * \brief Define the intrinsic permeability \f$\mathrm{[m^2]}\f$.
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
#if SPATIALPARAMS != 3
    Scalar intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvElemGeom,
                                 int scvIdx) const
#else
    const FieldMatrix& intrinsicPermeability(const Element &element) const
#endif
    {

        if(Dune::FloatCmp::lt<Scalar>(problemPtr_->timeManager().time(), initializationPeriod_) &&
                Dune::FloatCmp::lt<Scalar>(perm_[problemPtr_->elementMapper().map(element)], initialPermeability_))
        {
            return initialPermeability_; //high permeability for creating hydrostatic conditions
        }

        return perm_[problemPtr_->elementMapper().map(element)];
    }

    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx) const
    {
        return 0;
    }

    /*!
     * \brief Define the porosity \f$\mathrm{[-]}\f$.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
#if SPATIALPARAMS != 3
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx,
                    Scalar pressure) const
#else
    Scalar porosity(const Element &element) const
#endif
    {

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                    int dofIdxGlobal = problemPtr_->model().dofMapper().subIndex(element, scvIdx, dofCodim);
#else
                    int dofIdxGlobal = problemPtr_->model().dofMapper().map(element, scvIdx, dofCodim);
#endif

        //Get the reference porosity
        Scalar refPorosity = porosity_[problemPtr_->elementMapper().map(element)];

        //Return pressure independent porosity in initialization period
        if(problemPtr_->timeManager().episodeIndex() == 1)
        {
            refPorosity *= massReductionFactor_;
            return refPorosity;
        }

        //effective porosity calculation according to Schäfer et al. 2011
        Scalar refPressure = problemPtr_->initialPressureMap_.find(dofIdxGlobal)->second;
        Scalar X = compressibility_*(pressure - refPressure);
        Scalar effPorosity = refPorosity*(1 + X + std::pow(X,2)/2);
        return effPorosity;
    }


    // return the parameter object for the Brooks-Corey material law which depends on the position
#if SPATIALPARAMS == 2
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvElemGeom,
                                                int scvIdx) const
#elif SPATIALPARAMS == 3
    const MaterialLawParams& materialLawParams(const Element &element) const
#endif
#if SPATIALPARAMS != 1
    {
        return materialParams_;
    }
#endif

    /*!
     * \brief Define the dispersivity.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
    Scalar dispersivity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
        return 0;
    }

    bool useTwoPointGradient(const Element &element,
                             const int vertexI,
                             const int vertexJ) const
    {
        return false;
    }

    /*!
     * \brief Returns the heat capacity \f$[J/m^3 K]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
//    Scalar heatCapacity(const Element &element,
//                        const FVElementGeometry &fvGeometry,
//                        const int scvIdx) const
//    {
//        return
//            790 // specific heat capacity of granite [J / (kg K)]
//            * 2700 // density of granite [kg/m^3]
//            * (1 - porosity(element, fvGeometry, scvIdx));
//    }

    /*!
     * \brief Returns the thermal conductivity \f$[W/m^2]\f$ of the porous material.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
//    Scalar thermalConductivitySolid(const Element &element,
//                                    const FVElementGeometry &fvGeometry,
//                                    const int scvIdx) const
//    {
//        return lambdaSolid_;
//    }



    //Check if the vertex is at the transition zone, i.e. having both zec and other layer properties
    bool vertexAtSaltBoundary(const int vIdxGlobal) const
    {
//        if(std::binary_search(vIdxSB_.begin(), vIdxSB_.end(), vIdxGlobal))
//            return true;

        return getMarker(vIdxGlobal, vIdxSB);
    }

    //Check if the vertex has ter or quar layer neighbors
    bool vertexTerQuar(const int vIdxGlobal) const
    {
//        if(std::binary_search(vIdxTerQuar_.begin(), vIdxTerQuar_.end(), vIdxGlobal))
//            return true;

        return getMarker(vIdxGlobal, vIdxTerQuar);
    }

    //Find the element belonging to a river
    bool isRiver(const Element &element, const int scvIdx) const
    {
        int dofIdxGlobal;
        if(isBox)
        {
            dofIdxGlobal = problemPtr_->vertexMapper().map(element, scvIdx, dim);
            return getMarker(dofIdxGlobal, vIdxRiver);
        }
        dofIdxGlobal = gridView_.indexSet().index(element);
        return getMarker(dofIdxGlobal, eIdxRiver);

        //        if(std::binary_search(dofIdxRiver_.begin(), dofIdxRiver_.end(), dofIdxGlobal))
        //            return true;
    }


    //Find the elements belonging to the flux line
    int findFluxLineIdx(const Element &element) const
    {
        std::vector<Scalar> eParam = (*gridPtr_).parameters(element);
        return (int)(eParam[fluxElemIdx] + 0.5);
    }


    //Find the layer index from the grid file
    int findLayerIdx(const Element &element) const
    {
        int eIdxGlobal = gridView_.indexSet().index(element);
//        if(std::binary_search(eIdxTZ_.begin(), eIdxTZ_.end(), eIdx))
        if(getMarker(eIdxGlobal, eIdxTZ))
            return tzIdx;

        //Close the holes in the rupel if holesRupelClosed_ == true
        if(holesRupelClosed_ && findFluxLineIdx(element) == rupelFluxIdx)
            return rcIdx;
        //return the index of the layer so that it fits to the array strating with 0
        std::vector<Scalar> eParam = (*gridPtr_).parameters(element);
        return (int)(eParam[layerEIdx] + 0.5) - 1;
    }

    //return the value of elementMarkerMap_ according to the marker index
    bool getMarker(int globalIdx,  int markerIdx) const
    {
        //check whether element or vertex marker is required
        if(markerIdx == eIdxTZ || (markerIdx == eIdxRiver && !isBox))
        {
            return elementMarkerMap_.find(globalIdx)->second[markerIdx];
        }
        else if(markerIdx == vIdxSB || markerIdx == vIdxTerQuar || (markerIdx == vIdxRiver && isBox))
        {
            return vertexMarkerMap_.find(globalIdx)->second[markerIdx];
        }
        else
            DUNE_THROW(Dumux::NumericalProblem, "Wrong marker index at spatialparams");
    }



private:

    //Check if the vertex is at the transition zone, i.e. having both zec and other layer properties (internal function)
    bool vertexAtSaltBoundary_(const int vIdxGlobal) const
    {
        //Get the iterator over the neighbor elements
        NeighborElementSeedsIterator nESIt = vertexElementMapper_.vertexElementsBegin(vIdxGlobal);
        NeighborElementSeedsIterator nESItEnd = vertexElementMapper_.vertexElementsEnd(vIdxGlobal);
        bool hasSalt = false;
        bool hasNoSalt = false;
        for(; nESIt != nESItEnd; ++nESIt)
        {
            ElementPointer ePtr = this->gridView_.grid().entityPointer(*nESIt);
            std::vector<Scalar> eParam = (*gridPtr_).parameters(*ePtr);
            int layerIdx = ((int)(eParam[layerEIdx] + 0.5) - 1);
            if(layerIdx == zecIdx)
                hasSalt = true;
            else
                hasNoSalt = true;
        }
        if(hasSalt && hasNoSalt)
            return true;

        return false;
    }

    //Check if the vertex has ter or quar layer neighbors (internal function)
    bool vertexTerQuar_(const int vIdxGlobal) const
    {
        //Get the iterator over the neighbor elements
        NeighborElementSeedsIterator nESIt = vertexElementMapper_.vertexElementsBegin(vIdxGlobal);
        NeighborElementSeedsIterator nESItEnd = vertexElementMapper_.vertexElementsEnd(vIdxGlobal);
        bool hasTerQuar = false;
        for(; nESIt != nESItEnd; ++nESIt)
        {
            ElementPointer ePtr = this->gridView_.grid().entityPointer(*nESIt);
            std::vector<Scalar> eParam = (*gridPtr_).parameters(*ePtr);
            int layerIdx = ((int)(eParam[layerEIdx] + 0.5) - 1);
            if(layerIdx == terIdx || layerIdx == quarIdx)
                hasTerQuar = true;

        }

        return hasTerQuar;
    }

    //Find the element belonging to a river (internal function)
    bool isRiver_(const Element &element, const int vIdxGlobal) const
    {
        std::vector<Scalar> eParam = (*gridPtr_).parameters(element);
        int layerIdx = (int)(eParam[layerEIdx] + 0.5) - 1;
        if(dirichletAtTop_ || layerIdx != quarIdx)
        {
            return false;
        }
        else
        {
            // box
            if(isBox)
            {
                //Get the iterator over the neighbor elements
                NeighborElementSeedsIterator nESIt = vertexElementMapper_.vertexElementsBegin(vIdxGlobal);
                NeighborElementSeedsIterator nESItEnd = vertexElementMapper_.vertexElementsEnd(vIdxGlobal);
                bool hasRiver = false;
                bool hasNoRiver = false;
                for(; nESIt != nESItEnd; ++nESIt)
                {
                    ElementPointer ePtr = this->gridView_.grid().entityPointer(*nESIt);
                    std::vector<Scalar> eParam = (*gridPtr_).parameters(*ePtr);
                    if(eParam[riverElemIdx] > 0.5)
                        hasRiver = true;
                    else
                        hasNoRiver = true;
                }
                if(hasRiver && !hasNoRiver)
                    return true;

                return false;
            }
            // cell centered
            else
            {
                std::vector<Scalar> eParam = (*gridPtr_).parameters(element);
                if(eParam[riverElemIdx] > 0.5)
                    return true;
                else
                    return false;
            }

        }

    }

    //initialize Transition zone, rivers and salt boundary elements (internal function)
    void initializeTransitionZone_()
    {
        //loop over all vertices mark elements having a vertice both belonging to at least one zec and one other element as transition zone elements
        //first loop over all elements
        //exclude elements in the layer ubs and zec
        ElementIterator eIt = gridView_.template begin<0>();
        const ElementIterator eEndIt = gridView_.template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            int eIdxGlobal = gridView_.indexSet().index(*eIt);

            //Set the default to false
            if(isBox)
                elementMarkerMap_.insert(std::make_pair(eIdxGlobal, std::vector<bool>(1, false)));
            //cc
            else
                elementMarkerMap_.insert(std::make_pair(eIdxGlobal, std::vector<bool>(2, false)));

            std::vector<Scalar> eParam = (*gridPtr_).parameters(*eIt);
            int layerIdx = ((int)(eParam[layerEIdx] + 0.5) - 1);
            //get the number of vertices in the element
            int noCorner = eIt->geometry().corners();

            //loop over vertices of the element
            for (int vIdx = 0; vIdx < noCorner; ++vIdx)
            {

                //Get the global vertex index
                int vIdxGlobal = problemPtr_->vertexMapper().map(*eIt, vIdx, dim);
                std::map<int, std::vector<bool>>::iterator vHandle = vertexMarkerMap_.find(vIdxGlobal);
                // if vertex (number) was not found yet, insert new vertex entry with the number of markers according to
                // the discretization method used
                if (vHandle == vertexMarkerMap_.end())
                {
                    if(isBox)
                        vertexMarkerMap_.insert(std::make_pair(vIdxGlobal, std::vector<bool>(3, false)));
                    //cc
                    else
                        vertexMarkerMap_.insert(std::make_pair(vIdxGlobal, std::vector<bool>(2, false)));
                }

                //Check that the vertex is at a transition zone element and check if the vertex is at the salt boundary
                if(vertexAtSaltBoundary_(vIdxGlobal))
                {
                    //Check if the vertex is at the salt boundary
                    vertexMarkerMap_[vIdxGlobal][vIdxSB] = true;
//
//                    //Check if the element has already been marked and continue the for loop it that is the case
//                    //Transition zone elements should not be zec odr ubs
                    if(layerIdx != ubsIdx && layerIdx != zecIdx && layerIdx)
                        elementMarkerMap_[eIdxGlobal][eIdxTZ] = true;


                    //                        if(std::find(eIdxTZ_.begin(), eIdxTZ_.end(), eIdxGlobal) == eIdxTZ_.end())
                    //                        {
                    //                            eIdxTZ_.push_back(eIdxGlobal);
                    //                        }
                }

                //Check if dof is river
                if(isRiver_(*eIt, vIdxGlobal))
                {
                    if(isBox)
                        vertexMarkerMap_[vIdxGlobal][vIdxRiver] = true;
                    else
                        elementMarkerMap_[eIdxGlobal][eIdxRiver] = true;
                }
//                //Check if vertex is terquar
                if(vertexTerQuar_(vIdxGlobal))
                {
                    vertexMarkerMap_[vIdxGlobal][vIdxTerQuar] = true;
                }
            }
        }

        // Do another iteration of the elements to mark elements as TransitionZone zone elements
        // which are in between the ter layer and the TransitionZone layer at the top of the salt dome.
        // This is done to increase the contact area between TransitionZone zone and the ter layer.
        eIt = gridView_.template begin<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            int eIdxGlobal = gridView_.indexSet().index(*eIt);
            bool outsideElemIsTer = false;
            bool outsideElemIsTZ = false;
            IntersectionIterator isEndIt = gridView_.iend(*eIt);
            for (IntersectionIterator isIt = gridView_.ibegin(*eIt); isIt != isEndIt; ++isIt)
            {
                if(isIt->neighbor())
                {
                    ElementPointer outsideElemPtr(isIt->outside());
                    int outsideElemIdx = gridView_.indexSet().index(*outsideElemPtr);
                    int outsideLayerIdx = (int)((*gridPtr_).parameters(*outsideElemPtr)[1] + 0.5) - 1;
                    GlobalPosition normal = isIt->centerUnitOuterNormal();
                    Scalar normalZComp, absNormalZComp;
                    normalZComp = normal[dimWorld-1];
                    absNormalZComp = std::fabs(normalZComp);

                    //Top normal is ter
                    if(absNormalZComp > 0.2 )
                    {
                        //top intersection
                        if(normalZComp < 0 && outsideLayerIdx == terIdx)
                        {
                            outsideElemIsTer = true;
                        }
                        //bottom intersection
                        if(normalZComp > 0 && elementMarkerMap_[outsideElemIdx][eIdxTZ])
                        {
                            outsideElemIsTZ = true;
                        }
                    }
                }
            }
            if(outsideElemIsTer && outsideElemIsTZ)
            {
                elementMarkerMap_[eIdxGlobal][eIdxTZ] = true;
//                if(std::find(eIdxTZ_.begin(), eIdxTZ_.end(), eIdx) == eIdxTZ_.end())
//                {
//                    eIdxTZ_.push_back(eIdx);
//                }
            }
        }


        //            //Loop over all elements mark elements with interface zec/other
        //            ElementIterator eIt = gridView_.template begin<0>();
        //            const ElementIterator eEndIt = gridView_.template end<0>();
        //            for (; eIt != eEndIt; ++eIt)
        //            {
        //                std::vector<Scalar> eParam = (*gridPtr_).parameters(*eIt);
        //                //Check if the layer idx is zec
        //                if(((int)(eParam[layerEIdx] + 0.5) - 1) == zecIdx)
        //                {
        //                    //Loop over intersections
        //                    IntersectionIterator isEndIt = gridView_.iend(*eIt);
        //                    for (IntersectionIterator isIt = gridView_.ibegin(*eIt); isIt != isEndIt; ++isIt)
        //                    {
        //                        //Check if element has neighbor
        //                        if(isIt->neighbor())
        //                        {
        //                            ElementPointer outsideEPtr(isIt->outside());
        //                            int outsideEIdx = gridView_.indexSet().index*outsideEPtr);
        //                            std::vector<Scalar> eParam = (*gridPtr_).parameters(*outsideEPtr);
        //                            int layerIdx = ((int)(eParam[layerEIdx] + 0.5) - 1);
        //                            //Check if outside element is not zec
        //                            if(layerIdx != zecIdx && layerIdx != ubsIdx)
        //                            {
        //                                // store the element index of the transition zone element in the vector
        //                                // check if the elemnt index is already included in the vector
        //                                if(std::find(eIdxTZ_.begin(), eIdxTZ_.end(), outsideEIdx) == eIdxTZ_.end())
        //                                {
        //                                    eIdxTZ_.push_back(outsideEIdx);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //order the vectors in order to use binary_search for lookup
        // using default comparison:
//        std::sort (eIdxTZ_.begin(), eIdxTZ_.end());



        //Loop over all elements mark vertices with are river or salt boundary
//        for (; eIt != eEndIt; ++eIt)
//        {
//            //get the number of vertices in the element
//            int noCorner = eIt->geometry().corners();
//
//            //loop over vertices of the element
//            for (int vIdx = 0; vIdx < noCorner; ++vIdx)
//            {
//                //Get the global vertex index
//                int vIdxGlobal = problemPtr_->vertexMapper().map(*eIt, vIdx, dim);
//                std::vector<Scalar> eParam = (*gridPtr_).parameters(*eIt);
//                //Check that the vertex is at a salt boundary
//                if(vertexAtSaltBoundary_(vIdxGlobal))
//                {
//                    if(std::find(vIdxSB_.begin(), vIdxSB_.end(), vIdxGlobal) == vIdxSB_.end())
//                    {
//                        vIdxSB_.push_back(vIdxGlobal);
//                    }
//                }
//                //Check if the vertex is a river vertex
//                if(isRiver_(*eIt, vIdxGlobal))
//                {
//                    if(std::find(dofIdxRiver_.begin(), dofIdxRiver_.end(), vIdxGlobal) == dofIdxRiver_.end())
//                    {
//                        dofIdxRiver_.push_back(vIdxGlobal);
//                    }
//                }
//
//                //Check that the vertex has ter or quar elements
//                if(vertexTerQuar_(vIdxGlobal))
//                {
//                    if(std::find(vIdxTerQuar_.begin(), vIdxTerQuar_.end(), vIdxGlobal) == vIdxTerQuar_.end())
//                    {
//                        vIdxTerQuar_.push_back(vIdxGlobal);
//                    }
//                }
//            }
//        }

        //order the vectors in order to use binary_search for lookup
        // using default comparison:
//        std::sort (eIdxTZ_.begin(), eIdxTZ_.end());
//        std::sort (vIdxSB_.begin(), vIdxSB_.end());
//        std::sort (dofIdxRiver_.begin(), dofIdxRiver_.end());
//        std::sort (vIdxTerQuar_.begin(), vIdxTerQuar_.end());


        //               for(int i = 0; i < dofIdxRiver_.size(); i++)
        //               {
        //                   std::cout << dofIdxRiver_[i] << " ";
        //               }
        //               std::cout << "\n" << std::endl;


        //        std::map<int, std::vector<bool> > nodeMap;
        //
        //        int layerIdx;
        //        int noNodes;
        //        int globalNodeIdx;
        //        int localNodeIdx;
        //        bool outsideElemIsTer;
        //        bool outsideElemIsFault;
        //        GlobalPosition normal;
        //        int outsideLayerIdx;
        //        int outsideElemIdx;
        //
        //        ElementIterator eIt = gridView_.template begin<0>();
        //        const ElementIterator eEndIt = gridView_.template end<0>();
        //        //initialize node list with [false, false]
        //        for (; eIt != eEndIt; ++eIt)
        //        {
        //            //get the number of nodes in the element
        //            noNodes = eIt->geometry().corners();
        //
        //            //iterate over nodes of the element
        //            for (int localNodeIdx = 0; localNodeIdx < noNodes; ++localNodeIdx)
        //            {
        //                globalNodeIdx = problemPtr_->vertexMapper().map(*eIt, localNodeIdx, dim);
        //                //                std::cout<<"globalNodeIdx= "<<globalNodeIdx<<std::endl;
        //                nodeMap[globalNodeIdx] = {false, false};
        //            }
        //        }
        //
        //        // put values on the node list, if [true, true] the node is part of an element containing salt and
        //        // a regular formation element, i.e. it is at the border of the salt wall
        //        eIt = gridView_.template begin<0>();
        //        for (; eIt != eEndIt; ++eIt)
        //        {
        //            //get the number of nodes in the element
        //            noNodes = eIt->geometry().corners();
        //
        //            //iterate over nodes of the element
        //            for (localNodeIdx = 0; localNodeIdx < noNodes; ++localNodeIdx)
        //            {
        //                globalNodeIdx = problemPtr_->vertexMapper().map(*eIt, localNodeIdx, dim);
        //                layerIdx = (int)((*gridPtr_).parameters(*eIt)[1] + 0.5) - 1;
        //                if(layerIdx == zecIdx)
        //                {
        //                    nodeMap[globalNodeIdx][0] = true;
        //                }
        //                else
        //                {
        //                    nodeMap[globalNodeIdx][1] = true;
        //                }
        //            }
        //        }
        //
        //        //Every element containing a node at the border of the salt wall which is not salt
        //        eIt = gridView_.template begin<0>();
        //        for (; eIt != eEndIt; ++eIt)
        //        {
        //            int elemIdx = gridView_.indexSet().index*eIt);
        //            //get the number of nodes in the element
        //            noNodes = eIt->geometry().corners();
        //            elementFaultMap_[elemIdx] = false;
        //            layerIdx = (int)((*gridPtr_).parameters(*eIt)[1] + 0.5) - 1;
        //            //iterate over nodes of the element
        //            for (localNodeIdx = 0; localNodeIdx < noNodes; ++localNodeIdx)
        //            {
        //                globalNodeIdx = problemPtr_->vertexMapper().map(*eIt, localNodeIdx, dim);
        //                if(nodeMap[globalNodeIdx][0] == true && nodeMap[globalNodeIdx][1] == true
        //                        && layerIdx != zecIdx
        //                        && layerIdx != ubsIdx)
        //                {
        //                    elementFaultMap_[elemIdx] = true;
        //                }
        //            }
        //        }
        //
        //        //
        //        eIt = gridView_.template begin<0>();
        //        for (; eIt != eEndIt; ++eIt)
        //        {
        //            int elemIdx = gridView_.indexSet().index*eIt);
        //            outsideElemIsTer = false;
        //            outsideElemIsFault = false;
        //            IntersectionIterator isEndIt = gridView_.iend(*eIt);
        //            for (IntersectionIterator isIt = gridView_.ibegin(*eIt); isIt != isEndIt; ++isIt)
        //            {
        //                if(isIt->neighbor())
        //                {
        //                    ElementPointer outsideElemPtr(isIt->outside());
        //                    outsideElemIdx = gridView_.indexSet().index*outsideElemPtr);
        //                    outsideLayerIdx = (int)((*gridPtr_).parameters(*outsideElemPtr)[1] + 0.5) - 1;
        //                    normal = isIt->centerUnitOuterNormal();
        //                    Scalar normalZComp, absNormalZComp;
        //                    normalZComp = normal[dimWorld-1];
        //                    absNormalZComp = std::fabs(normalZComp);
        //
        //                    //Top normal is ter
        //                    if(absNormalZComp > 0.2 )
        //                    {
        //                        //top intersection
        //                        if(normalZComp < 0 && outsideLayerIdx == terIdx)
        //                        {
        //                            outsideElemIsTer = true;
        //                        }
        //                        //bottom intersection
        //                        if(normalZComp > 0 && elementFaultMap_.find(outsideElemIdx)->second)
        //                        {
        //                            outsideElemIsFault = true;
        //                        }
        //                    }
        //                }
        //            }
        //            if(outsideElemIsTer && outsideElemIsFault)
        //            {
        //                elementFaultMap_[elemIdx] = true;
        //            }
        //
        //        }
    }


    // ReservoirProperties
    static constexpr int numLayer_ = 12;
    Scalar arrayTopLayer_[numLayer_];
    Scalar topOfReservoir_;
    Scalar tzWidth_;
    Scalar cellWidth_;
    Scalar initialPermeability_;
    Scalar arrayPerm_[numLayer_];
    Scalar arrayPor_[numLayer_];

    //Vectors storing permeability and porosity values at every element for faster access
    PermeabilityType perm_;
    PorosityType porosity_;

    bool holesRupelClosed_;
    bool dirichletAtTop_;

    //Two phase parameters
#if SPATIALPARAMS != 1
    MaterialLawParams materialParams_;
#endif
    Scalar Swr_;
    Scalar Snr_;
    Scalar brooksCoreyPe_;
    Scalar brooksCoreyLambda_;
    Scalar compressibility_;

    // problem pointer for obtaining the solution
    Problem *problemPtr_;

    //gridPtr_ for accessing parameters in dgf
    const GridView gridView_;
    GridPointer *gridPtr_;

    //Initialization period
    Scalar initializationPeriod_;


    //Mass reduction factor for multiplication with porosity
    Scalar massReductionFactor_;

    //key: element index, value: is fault element?
//    std::map<int, bool> elementFaultMap_;

    //Map storing the element information
    std::map<int, std::vector<bool>> elementMarkerMap_;
    std::map<int, std::vector<bool>> vertexMarkerMap_;
//    std::vector<int> eIdxTZ_;
//    std::vector<int> vIdxSB_;
//    std::vector<int> dofIdxRiver_;
//    std::vector<int> vIdxTerQuar_;

    // Mapper for assigning neighbor elements (value) to global vertex indices
    VertIdxToElemNeighborMapper vertexElementMapper_;

};

}

#endif
