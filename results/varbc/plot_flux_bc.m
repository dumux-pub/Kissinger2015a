%Plot the flux through the fault zone into the top aquifer for the complex geometry
%model using the outputReader.m
% scenario: variable boundary conditions

clear all;
plotName = 'closedcompare';

%define the indices for result and search lists
timeIdx = 1;
fluxFaultIdx = 2;
fluxRupelIdx = 3;
fluxHolesIdx = 4;
fluxTotalIdx = 5;

%calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the nuber of log-files to be evaluated
% - Subract the base flux at tinit from all fluxes, multiply by -1
%   to give the flux a positive value; 
% - divide by the injection rate to make
% - the flux dimensionless
tinit = 1e9;
injectionRate = 23.208;
%array of logfiles to read
logFileName{1} = 'tz_1e12.log';
logFileName{2} = 'closed.log';
%store the data from the logfile in outmat.mat
outputMatName = 'outmat.mat';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time:';
strList{fluxFaultIdx} = 'Flux Across Fault: Total Flux:';
strList{fluxRupelIdx} = 'Flux Across Rupel: Total Flux:';
strList{fluxHolesIdx} = 'Flux Across Holes Rupel: Total Flux:';
strList{fluxTotalIdx} = 'Flux into Ter Quar: Total Flux:';

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    outputReader(strList, logFileName{logIter}, outputMatName);
    load(outputMatName);
    tInitIdx = find((output(:, timeIdx) - tinit) == 0);
    time{logIter} = (output(tInitIdx:length(output), timeIdx) - tinit)/365/24/3600;
    fluxTotal{logIter} = (output(tInitIdx:length(output), fluxTotalIdx) - output(tInitIdx, fluxTotalIdx))*(-1)/injectionRate;
    fluxHoles{logIter} = (output(tInitIdx:length(output), fluxHolesIdx) - output(tInitIdx, fluxHolesIdx))*(-1)/injectionRate;
    fluxFault{logIter} = (output(tInitIdx:length(output), fluxFaultIdx) - output(tInitIdx, fluxFaultIdx))*(-1)/injectionRate;
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100;
strInterpreter = 'latex';
iFontSize = 9;
set(gcf, 'Units', 'centimeters');
Fig = figure(...
    'PaperUnits','centimeters', ...
     'PaperPosition',[0 , 0, 8, 11]); 

for logIter = 1:length(logFileName)
    fluxplot = plot(time{logIter}(time{logIter}<=timePlot), fluxTotal{logIter}(time{logIter}<=timePlot), ...
        time{logIter}(time{logIter}<=timePlot), fluxFault{logIter}(time{logIter}<=timePlot));
    if(logIter == 1)
        set(fluxplot(1), 'color', [0 0 1]);
        set(fluxplot(2), 'color', [0 0.5 0]);
        set(fluxplot(1), 'LineStyle', '--');
        set(fluxplot(2), 'LineStyle', '--');
    else
        set(fluxplot(1), 'color', [0 0 1]);
        set(fluxplot(2), 'color', [0 0.5 0]);
    end
    
    set(fluxplot, 'LineWidth',1);
    hold on;
end

h_leg = legend('Offen: Gesamtfluss~~~~~~~~~~~~~~~~~~~~~~~~~',...
               'Offen: Fluss \"uber Salzflanke~~~~~~~~~~~~~', ...
               'Geschlossen: Gesamtfluss~~~~~~~~~~~~~~~~~~~', ...
               'Geschlossen: Fluss \"uber Salzflanke~~~~~~~', ...
               'Location', 'southoutside');
set(h_leg,'Interpreter',strInterpreter, 'FontSize', iFontSize);
ylabel('Massenfluss / Injektionsrate [-]', 'FontSize', iFontSize);
xlabel('Zeit [Jahren]', 'FontSize', iFontSize);
%Set the font size of the axis ticks
set(gca,'FontSize',iFontSize)
axis([0,100,0,1])
axis square;
ax = gca;
set(ax, 'YTick', [0, 0.2, 0.4, 0.6, 0.8, 1.0]);
grid on;

saveas(Fig,plotName,'epsc') 

