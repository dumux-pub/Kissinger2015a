%Plot the flux through the fault zone into the top aquifer for the complex geometry
%model using the outputReader.m.
% Scenario variable barrier permeability

clear all;
% plotName{1} = 'bar_1e17';
% plotName{2} = 'bar_1e18';
% plotName{3} = 'bar_1e19';
% plotName{4} = 'bar_1e20';
plotName = 'varbar';

%define the indices for result and search lists
timeIdx = 1;
fluxFaultIdx = 2;
fluxRupelIdx = 3;
fluxHolesIdx = 4;
fluxTotalIdx = 5;

%calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the nuber of log-files to be evaluated
% - Subract the base flux at tinit from all fluxes, multiply by -1
%   to give the flux a positive value; 
% - divide by the injection rate to make
% - the flux dimensionless
tinit = 1e9;
injectionRate = 23.208;
%array of logfiles to read
logFileName{1} = 'bar_1e17.log';
logFileName{2} = 'tz_1e12.log';
logFileName{3} = 'bar_1e19.log';
logFileName{4} = 'bar_1e20.log';
titel{1} = 'K = 10$^{-17}$ m$^2$';
titel{2} = 'K = 10$^{-18}$ m$^2$';
titel{3} = 'K = 10$^{-19}$ m$^2$';
titel{4} = 'K = 10$^{-20}$ m$^2$';



%store the data from the logfile in outmat.mat
outputMatName = 'outmat.mat';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time:';
strList{fluxFaultIdx} = 'Flux Across Fault: Total Flux:';
strList{fluxRupelIdx} = 'Flux Across Rupel: Total Flux:';
strList{fluxHolesIdx} = 'Flux Across Holes Rupel: Total Flux:';
strList{fluxTotalIdx} = 'Flux into Ter Quar: Total Flux:';

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    outputReader(strList, logFileName{logIter}, outputMatName);
    load(outputMatName);
    tInitIdx = find((output(:, timeIdx) - tinit) == 0);
    time{logIter} = (output(tInitIdx:length(output), timeIdx) - tinit)/365/24/3600;
    fluxTotal{logIter} = (output(tInitIdx:length(output), fluxTotalIdx) - output(tInitIdx, fluxTotalIdx))*(-1)/injectionRate;
    fluxHoles{logIter} = (output(tInitIdx:length(output), fluxHolesIdx) - output(tInitIdx, fluxHolesIdx))*(-1)/injectionRate;
    fluxFault{logIter} = (output(tInitIdx:length(output), fluxFaultIdx) - output(tInitIdx, fluxFaultIdx))*(-1)/injectionRate;
    
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100;
strInterpreter = 'latex';
iFontSize = 9;
set(gcf, 'Units', 'centimeters');
Fig = figure(...
    'PaperUnits','centimeters', ...
     'PaperPosition',[0 , 0, 19, 17]); 
        %'PaperType','a4letter',...
        %'PaperOrientation','Portrait',...
        %'PaperUnits','centimeters', ...
        %'ActivePositionProperty','Position', ....
        %'PaperPosition',[0 , 0, 15.89, 15.89]); 
        %'PaperPosition',[2.54 , 2.54, 18.43, 18.43]); 

for logIter = 1:length(logFileName)

    subplot(2,2,logIter);
    fluxplot = plot(time{logIter}(time{logIter}<=timePlot), fluxTotal{logIter}(time{logIter}<=timePlot), ...
        time{logIter}(time{logIter}<=timePlot), fluxFault{logIter}(time{logIter}<=timePlot), ...
        time{logIter}(time{logIter}<=timePlot), fluxHoles{logIter}(time{logIter}<=timePlot));
    set(fluxplot, 'LineWidth',1.0);
    h_title = title(titel{logIter},'FontSize', iFontSize);
    set(h_title,'Interpreter',strInterpreter)
    ylabel('Massenfluss / Injektionsrate [-]', 'FontSize', iFontSize);
    xlabel('Zeit[Jahren]', 'FontSize', iFontSize);
    %Set the font size of the axis ticks
    set(gca,'FontSize',iFontSize)
    axis([0,100,0,1])
    axis square;
    grid on;
    if(logIter==1)
        h_leg = legend('Gesamtfluss', 'Fluss \"uber Salzflanke', 'Fluss \"uber Rupelfehlstellen','Location', 'NorthEast');
        set(h_leg,'Interpreter',strInterpreter, 'FontSize', iFontSize);
        
    end

end


%set(Fig,'PaperPositionMode','Auto')
saveas(Fig,plotName,'epsc')

