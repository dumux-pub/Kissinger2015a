%Plot the flux into the top aquifer for the complex geometry
%model using the outputReader.m
% scenario variable permeability of transition zone (Störungszone)

clear all;
plotName = 'vartz';

%define the indices for result and search lists
timeIdx = 1;
fluxFaultIdx = 2;
fluxRupelIdx = 3;
fluxHolesIdx = 4;
fluxTotalIdx = 5;

%initalization time and injection rate, check from dumux input file or end
%of log-file
tinit = 1e9;
injectionRate = 23.208;
%array of logfiles to read
logFileName{1} = 'tz_1e11.log';
logFileName{2} = 'tz_1e12.log';
logFileName{3} = 'tz_1e13.log';
logFileName{4} = 'tz_1e14.log';
logFileName{5} = 'tz_1e15.log';
logFileName{6} = 'tz_1e16.log';
logFileName{7} = 'tz_1e17.log';
logFileName{8} = 'tz_1e18.log';
logFileName{9} = 'tz_1e19.log';
perm = [1e-11 1e-12 1e-13 1e-14 1e-15 1e-16 1e-17 1e-18 1e-19];
%store the data from the logfile in outmat.mat
outputMatName = 'outmat.mat';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time:';
strList{fluxFaultIdx} = 'Flux Across Fault: Total Flux:';
strList{fluxRupelIdx} = 'Flux Across Rupel: Total Flux:';
strList{fluxHolesIdx} = 'Flux Across Holes Rupel: Total Flux:';
strList{fluxTotalIdx} = 'Flux into Ter Quar: Total Flux:';


%calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the nuber of log-files to be evaluated
% - Subract the base flux at tinit from all fluxes, multiply by -1
%   to give the flux a positive value; 
% - divide by the injection rate to make
% - the flux dimensionless
for logIter = 1:length(logFileName)
    outputReader(strList, logFileName{logIter}, outputMatName);
    load(outputMatName);
    tInitIdx = find((output(:, timeIdx) - tinit) == 0);
    time{logIter} = (output(tInitIdx:length(output), timeIdx) - tinit)/365/24/3600;
    fluxTotal{logIter} = (output(tInitIdx:length(output), fluxTotalIdx) - output(tInitIdx, fluxTotalIdx))*(-1)/injectionRate;
    fluxHoles{logIter} = (output(tInitIdx:length(output), fluxHolesIdx) - output(tInitIdx, fluxHolesIdx))*(-1)/injectionRate;
    fluxFault{logIter} = (output(tInitIdx:length(output), fluxFaultIdx) - output(tInitIdx, fluxFaultIdx))*(-1)/injectionRate;
    maxFlux(logIter) = max(fluxFault{logIter});
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100;
strInterpreter = 'latex';
iFontSize = 9;
set(gcf, 'Units', 'centimeters');
Fig = figure(...
    'PaperUnits','centimeters', ...
     'PaperPosition',[0 , 0, 8, 8]); 

fluxplot = semilogx(perm, maxFlux);
set(fluxplot, 'LineWidth',1);
ylabel('Massenfluss / Injektionsrate [-]', 'FontSize', iFontSize);
xlabel('Permeabilit\"at [m$^2$]', 'Interpreter',strInterpreter, 'FontSize', iFontSize);
%Set the font size of the axis ticks
set(gca,'FontSize',iFontSize)
axis([10^-19,10^-11,0,1])
axis square;
ax = gca;
set(ax, 'YTick', [0, 0.2, 0.4, 0.6, 0.8, 1.0]);
set(ax, 'XTick', [1e-19, 1e-17, 1e-15, 1e-13, 1e-11]);
grid on;
saveas(Fig,plotName,'epsc') 

% for logIter = 1:length(logFileName)
%     fluxplot(logIter) = plot(time{logIter}(time{logIter}<=timePlot), fluxFault{logIter}(time{logIter}<=timePlot));
%     set(fluxplot(logIter), 'LineWidth',2);
%     hold on;
% end
% set(fluxplot(1), 'color', [0 0 1]);
% set(fluxplot(2), 'color', [0 0.5 0]);
% set(fluxplot(3), 'color', [1 0 0]);
% h_title = title('Fluss in oberen Aquifer f\"ur verschiedene Injektionsschicht Permeabilit\"aten');
% set(h_title,'Interpreter','latex')
% h_leg = legend('K=$10^{-12}$ m$^2$', 'K=$10^{-13}$ m$^2$', 'K=$10^{-14}$ m$^2$', 'Location', 'NorthEast');
% set(h_leg,'Interpreter','latex')
% ylabel('Massenfluss / Injektionsrate [-]');
% xlabel('Zeit [Jahren]');
% axis([0,100,0,1])
% axis square;

