%Plot the flux through the fault zone into the top aquifer for the complex geometry
%model using the outputReader.m
% reference scenario: transition zone permeability 1e-12 m2

clear all;
plotName = 'tz1e12';

%define the indices for result and search lists
timeIdx = 1;
fluxFaultIdx = 2;
fluxRupelIdx = 3;
fluxHolesIdx = 4;
fluxTotalIdx = 5;

%initalization time and injection rate, check from dumux input file or end
%of log-file
tinit = 1e9;
injectionRate = 23.208;
%array of logfiles to read
logFileName{1} = 'tz_1e12.log';

%store the data from the logfile in outmat.mat
outputMatName = 'outmat.mat';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time:';
strList{fluxFaultIdx} = 'Flux Across Fault: Total Flux:';
strList{fluxRupelIdx} = 'Flux Across Rupel: Total Flux:';
strList{fluxHolesIdx} = 'Flux Across Holes Rupel: Total Flux:';
strList{fluxTotalIdx} = 'Flux into Ter Quar: Total Flux:';

%calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the nuber of log-files to be evaluated
% - Subract the base flux at tinit from all fluxes, multiply by -1
%   to give the flux a positive value; 
% - divide by the injection rate to make
% - the flux dimensionless
for logIter = 1:length(logFileName)
    outputReader(strList, logFileName{logIter}, outputMatName);
    load(outputMatName);
    tInitIdx = find((output(:, timeIdx) - tinit) == 0);
    time{logIter} = (output(tInitIdx:length(output), timeIdx) - tinit)/365/24/3600;
    fluxTotal{logIter} = (output(tInitIdx:length(output), fluxTotalIdx) - output(tInitIdx, fluxTotalIdx))*(-1)/injectionRate;
    fluxHoles{logIter} = (output(tInitIdx:length(output), fluxHolesIdx) - output(tInitIdx, fluxHolesIdx))*(-1)/injectionRate;
    fluxFault{logIter} = (output(tInitIdx:length(output), fluxFaultIdx) - output(tInitIdx, fluxFaultIdx))*(-1)/injectionRate;
    fluxRupel{logIter} = fluxTotal{logIter} - fluxHoles{logIter} - fluxFault{logIter};
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100;
strInterpreter = 'latex';
iFontSize = 9;
set(gcf, 'Units', 'centimeters');
Fig = figure(...
    'PaperUnits','centimeters', ...
     'PaperPosition',[0 , 0, 8, 8]); 

for logIter = 1:length(logFileName)
    %fluxplot(logIter) = plot(time{logIter}(time{logIter}<=timePlot), fluxFault{logIter}(time{logIter}<=timePlot));
    fluxplot = plot(time{logIter}(time{logIter}<=timePlot), fluxTotal{logIter}(time{logIter}<=timePlot), ...
        time{logIter}(time{logIter}<=timePlot), fluxFault{logIter}(time{logIter}<=timePlot), ...
        time{logIter}(time{logIter}<=timePlot), fluxHoles{logIter}(time{logIter}<=timePlot), ...
        time{logIter}(time{logIter}<=timePlot), fluxRupel{logIter}(time{logIter}<=timePlot));
    set(fluxplot, 'LineWidth',1.0);
end
    set(fluxplot(1), 'color', [0 0 1]);
    set(fluxplot(2), 'color', [0 0.5 0]);
    set(fluxplot(3), 'color', [1 0 0]);
    set(fluxplot(4), 'color', [0.5 0.2 0]);
    %h_title = title(titel{logIter});
    %set(h_title,'Interpreter','latex')
    ylabel('Massenfluss / Injektionsrate [-]', 'FontSize', iFontSize);
    xlabel('Zeit[Jahren]', 'FontSize', iFontSize);
    h_leg = legend('Gesamtfluss~~~~~~~~~~~~~~~~~~~~~', ...
                   'Fluss \"uber Salzflanke~~~~~~~~~', ...
                   'Fluss \"uber Rupelfehlstellen~~~', ...
                   'Fluss \"uber Rupelium (intakt)~~',...
                   'Location', 'NorthEast');
    set(h_leg,'Interpreter',strInterpreter, 'FontSize', iFontSize);
    
    %Set the font size of the axis ticks
    set(gca,'FontSize',iFontSize)
    axis([0,100,0,1])
    axis square;
    ax = gca;
    set(ax, 'YTick', [0, 0.2, 0.4, 0.6, 0.8, 1.0]);
    set(ax, 'XTick', [0, 20, 40, 60, 80, 100]);
    grid on;
    saveas(Fig,plotName,'epsc') 




