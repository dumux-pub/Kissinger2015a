% Read output files at every time step and write them into a mat file
%
% Input Parameters:
% strList = String list to look for. Mapping where time should be the first
% parameter.
% logFileName = Name of log file to go through
% outputMatName = Name of the output mat file
% files
%
% Output Parameters:
% success = return 1 if successful else 0

function outputReader(strList, logFileName, outputMatName)
        
%get the time for after each time-step
lineIter = 0;
timeIter = 0;
fid = fopen(logFileName, 'r');
while feof(fid) == 0
    lineIter =lineIter+1;
    line = fgetl(fid);

    for strListIter = 1:length(strList)
        if(strfind(line, strList{strListIter})~=0)
            if(strListIter == 1)
                timeIter = timeIter + 1;
            end
            str = line(strfind(line, strList{strListIter}) ...
                +length(strList{strListIter}):strfind(line, ';')-1);
            output(timeIter, strListIter) = str2double(str);
        end
    end   
end
fclose(fid);

fprintf('> > > >Succesfully read %s \n', logFileName);

save(outputMatName, 'output');

fprintf('> > > >Succesfully wrote %s \n', outputMatName);


end



        
