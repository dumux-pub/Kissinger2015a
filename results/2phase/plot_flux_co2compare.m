%Plot the flux through the fault zone into the top aquifer for the complex geometry
%model using the outputReader.m
% scenario: compare flux for 1phase and 2phase model

clear all;
plotName = 'co2compare';

%define the indices for result and search lists
timeIdx = 1;
fluxFaultIdx = 2;
fluxRupelIdx = 3;
fluxHolesIdx = 4;
fluxTotalIdx = 5;

%initalization time and injection rate, check from dumux input file or end
%of log-file
tinit = 1e9;
injectionRate = 23.208;
%array of logfiles to read
logFileName{1} = 'tz_1e12.log';
logFileName{2} = '2phase.log';
%store the data from the logfile in outmat.mat
outputMatName = 'outmat.mat';

%the list of strings to be searched in the log file
strList{1}{timeIdx} = 'Time:';
strList{1}{fluxFaultIdx} = 'Flux Across Fault: Total Flux:';
strList{1}{fluxRupelIdx} = 'Flux Across Rupel: Total Flux:';
strList{1}{fluxHolesIdx} = 'Flux Across Holes Rupel: Total Flux:';
strList{1}{fluxTotalIdx} = 'Flux into Ter Quar: Total Flux:';

strList{2}{timeIdx} = 'Time:';
strList{2}{fluxFaultIdx} = 'Flux Across Fault (holes in Rupel and saltwall): Water Flux:';
strList{2}{fluxRupelIdx} = 'Flux Across Rupel: Water Flux:';
strList{2}{fluxHolesIdx} = 'Flux Across Holes Rupel: Water Flux:';
strList{2}{fluxTotalIdx} = 'Flux into Ter Quar: Water Flux:';


%calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the nuber of log-files to be evaluated
% - Subract the base flux at tinit from all fluxes, multiply by -1
%   to give the flux a positive value; 
% - divide by the injection rate to make
% - the flux dimensionless
for logIter = 1:length(logFileName)
    outputReader(strList{logIter}, logFileName{logIter}, outputMatName);
    load(outputMatName);
    tInitIdx = find((output(:, timeIdx) - tinit) == 0);
    time{logIter} = (output(tInitIdx:length(output), timeIdx) - tinit)/365/24/3600;
    fluxTotal{logIter} = (output(tInitIdx:length(output), fluxTotalIdx) - output(tInitIdx, fluxTotalIdx))*(-1)/injectionRate;
    fluxHoles{logIter} = (output(tInitIdx:length(output), fluxHolesIdx) - output(tInitIdx, fluxHolesIdx))*(-1)/injectionRate;
    fluxFault{logIter} = (output(tInitIdx:length(output), fluxFaultIdx) - output(tInitIdx, fluxFaultIdx))*(-1)/injectionRate;
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100;
strInterpreter = 'latex';
iFontSize = 9;
set(gcf, 'Units', 'centimeters');
Fig = figure(...
    'PaperUnits','centimeters', ...
     'PaperPosition',[0 , 0, 8, 8]); 

for logIter = 1:length(logFileName)
    fluxplot(logIter) = plot(time{logIter}(time{logIter}<=timePlot), fluxFault{logIter}(time{logIter}<=timePlot));
    set(fluxplot(logIter), 'LineWidth',1);
    hold on;
end
set(fluxplot(1), 'color', [0 0 1]);
set(fluxplot(2), 'color', [0 0.5 0]);
h_leg = legend('Wasserinjektion', 'CO$_2$-Injektion', 'Location', 'NorthEast');
set(h_leg,'Interpreter',strInterpreter, 'FontSize', iFontSize);
ylabel('Massenfluss / Injektionsrate [-]', 'FontSize', iFontSize);
xlabel('Zeit [Jahren]', 'FontSize', iFontSize);
axis([0,100,-0.1,1])
axis square;
%Set the font size of the axis ticks
set(gca,'FontSize',iFontSize)
ax = gca;
set(ax, 'YTick', [-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1.0]);
set(ax, 'XTick', [0, 20, 40, 60, 80, 100]);
grid on;

saveas(Fig,plotName,'epsc') 

