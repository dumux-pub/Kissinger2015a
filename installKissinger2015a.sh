#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### DUNE modules
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-common.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-grid.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-istl.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-localfunctions.git

### alugrid
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
cd dune-alugrid
git checkout b97734c1a40f965ed551f6ff04b6fe4933ee045e
cd ..

### dumux
git clone -b releases/2.7 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git

git clone -b master https://git.iws.uni-stuttgart.de/dumux-pub/Kissinger2015a.git dumux-Kissinger2015a

### dunecontrol
./dune-common/bin/dunecontrol --opts=Kissinger2015a.opts all
